#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;
 
namespace
{
  // Hello - The first implementation, without getAnalysisUsage.
   struct ItrInsideBB : public FunctionPass
   {
    
	static char ID; // Pass identification, replacement for typeid
    	ItrInsideBB() : FunctionPass(ID) {}

	bool runOnFunction(Function &F) override{
	errs() << "Hello: ";
	errs() << F.getName() <<"\n";
	 for(Function::iterator bb = F.begin(), e = F.end(); bb !=e; bb++)
        	{
	        	errs() << "Basicblock name=" << bb->getName()<<"\n";
		        errs() << "Basicblock Size=" << bb->size()<<"\n\n";
		for(BasicBlock::iterator i = bb->begin(), i1 = bb->end(); i != i1; i++)
		{
			errs() << " " << *i <<"\n";

		}
	     }
	}
   };
}

char ItrInsideBB::ID = 0;
static RegisterPass<ItrInsideBB> X("ItrInsideBB", "Iterate inside bb");
