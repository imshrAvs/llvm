//===- Count_Opcodes.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Instructions.h" 
#include "llvm/IR/InstrTypes.h" 
#include "llvm/IR/CallSite.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Module.h"
#include <map>
#include <vector>
#include "llvm/Analysis/LoopAccessAnalysis.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/Analysis/DependenceAnalysis.h"
//include "llvm/IR/ScalarEvolution.h"
using namespace llvm;

//STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct Scev : public FunctionPass {
    
    std::map<int,SCEV *> gep;
    std::map<SCEV *,Instruction*> mi;
    std::map<int ,SCEV *> vo;
    std::vector<llvm::SCEV*> Offset;
    std::vector<int> E_array; 
    std::map<int,Instruction *> com;

    static char ID; // Pass identification, replacement for typeid
    Scev() : FunctionPass(ID) {}
    

    void getAnalysisUsage(AnalysisUsage &AU) const override {  
    
      AU.addRequired<ScalarEvolutionWrapperPass>();
    }

    virtual bool runOnFunction(Function &F) override {
    ScalarEvolution  *SE = &getAnalysis<ScalarEvolutionWrapperPass>().getSE();
      //AU.addRequired<ScalarEvolutionWrapperPass>();
      //const SCEV *Scev0 = SE.getSCEV(Ptr);
      errs() << "Function: " << F.getName() << "\n";
      errs() << "Number of Arguments in a Function: " << F.arg_size() << "\n\n";
      
      
      
       IRBuilder<> Builder((F.begin())->getFirstNonPHI());
      
      int count=0; 
        SCEV *Scev0;
        SCEV *Diff; 
      for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb){
          
        for (Instruction &I : *bb){

           if((dyn_cast<GetElementPtrInst>(&I))){
               Scev0=const_cast<SCEV *>(SE->getSCEV(&I));
               SCEV *BaseP=const_cast<SCEV *>(SE->getPointerBase(Scev0));
     
               count++;
          //     errs() << "Original scev instruction:" << "\t" << *Scev0 << "\n";
            //    errs() << "Base:" << "\t" << *BaseP << "\n";
               
               Diff=const_cast<SCEV *>(SE->getMinusSCEV(Scev0,BaseP));
              // errs() << "Difference:" <<"\t" << *Diff << "\n";
               Offset.push_back(Diff);
               mi[Diff]=&I;

               errs() << "\n" ;


           }
         }
      }


      for(std::map<SCEV *,Instruction *>::iterator V=mi.begin(), e=mi.end(); V !=e;++V){
           errs()<< *V->first << " :" << *V->second << "\n";
      }

      errs() << "\n\n";

      SCEV *ele;
      
      for(std::vector< SCEV*>::iterator V=Offset.begin(), e=Offset.end(); V !=e;++V)
      {
       SCEVAddRecExpr *AddRec = dyn_cast<SCEVAddRecExpr>(*V);
       ele = const_cast<SCEV *>(AddRec->getStart());
      SCEVConstant *Bconst = dyn_cast<SCEVConstant>(ele);
       ConstantInt *CI = Bconst->getValue();
       int Value= CI->getSExtValue(); 
       E_array.push_back(Value);
      vo[Value]=*V;

      }
     

      for(std::map<int,SCEV *>::iterator V=vo.begin(), e=vo.end(); V !=e;++V){
              errs()<< V->first << " :" << *V->second << "\n\n";
      }


  /*    Instruction *base,*base1,*base2;
      for(std::map<SCEV*,Instruction *>::iterator V=mi.begin(),e=mi.end(); V!=e;++V){
           base =dyn_cast<Instruction>(V->second) ;
           errs() << *base << "\n";
           break;
      }
    
      errs() << "\n\n";
      int c=0;
      Instruction *inst;*/
       
      Instruction *base,*var,*inst;


      for(std::map<int,SCEV *>::iterator V1=vo.begin(), e1=vo.end();V1 !=e1;++V1){
              
              
                base=dyn_cast<Instruction>(mi[V1->second]);

              if((std::next(V1))!=vo.end())
              {
              var = dyn_cast<Instruction>(mi[(std::next(V1))->second]);

              errs() << *base << "\n";
              errs() << *var << "\n";

              { 
                 for(Use &U: (*var).operands()){
                    Value *v = U.get();
                    inst= dyn_cast<Instruction>(v);
                    if(!inst)
                    continue;
                    foo(inst,base);
                    inst->moveBefore(base);
                  }
             }

              (*(mi[(std::next(V1))->second])).moveAfter(base);
              }
              
            
         }
      
/*
      for(std::map<SCEV*,Instruction *>::iterator V=mi.begin(),e=mi.end(); V!=e;++V){
          c++;

        if(c!=3)
          continue;
        if(c==3) {

           base1 =dyn_cast<Instruction>(V->second) ;

          { 
          for(Use &U: (*V->second).operands()){

                Value *v = U.get();
                inst= dyn_cast<Instruction>(v);
                if(!inst)
                  continue;
               foo(inst,base);

                inst->moveBefore(base);

               }
          }
                    

              (*V->second).moveAfter(base);
           break;
           }
        } 
    
       
 int cc=0;
      for(std::map<SCEV*,Instruction *>::iterator V=mi.begin(),e=mi.end(); V!=e;++V){
        cc++;
        if(cc!=4)
          continue;
        if( cc==4){
       
            base2=dyn_cast<Instruction>(V->second);
          { 
          for(Use &U: (*V->second).operands()){

                Value *v = U.get();
                inst= dyn_cast<Instruction>(v);
                if(!inst)
                  continue;
               foo(inst,base1);

                inst->moveBefore(base1);

               }
          }

              (*V->second).moveAfter(base1);
            break;
        }
      }



 int ccc=0;
      for(std::map<SCEV*,Instruction *>::iterator V=mi.begin(),e=mi.end(); V!=e;++V){
        ccc++;
        if(ccc!=2)
          continue;
        if( cc==2){
       
          { 
          for(Use &U: (*V->second).operands()){

                Value *v = U.get();
                inst= dyn_cast<Instruction>(v);
                if(!inst)
                  continue;
               foo(inst,base2);

                inst->moveBefore(base2);

               }
          }

              (*V->second).moveAfter(base2);
            break;
        }
      }
*/
     errs() << "Function " << F << "\n";    
     return false;
  
  }
  
    
  void foo(Instruction *inst,Instruction *base) {
       if (inst->getParent()  != base->getParent())
         return ;
       for(Use &use : inst->operands()) {
         Value *uu= use.get();
         Instruction *i = dyn_cast<Instruction>(uu);
           if (!i)
             continue;
         if (base->getParent() != i->getParent())
             continue;
         isa<PHINode>(i);
           continue;
         {
             i->moveBefore(base);
             foo(i,inst);
         }
        }

    }

    };

}
char Scev::ID = 0;
static RegisterPass<Scev> X("Scev", "Scalar Evolutionn");



