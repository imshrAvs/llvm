//===- Count_Opcodes.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;
#include <map>
#define DEBUG_TYPE "opCounter"

//STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct UseDef : public FunctionPass {
    		
    static char ID; // Pass identification, replacement for typeid
    UseDef() : FunctionPass(ID) {}

     bool runOnFunction(Function &F) override {
      //++HelloCounter;
      errs() << "Function" << F.getName() << "\n";
      for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
	for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i !=e; ++i) {
        
		Instruction* inst = dyn_cast<Instruction>(i);
		if(inst->getOpcode() == Instruction::Add)
		{
			for(Use &U: inst->operands())
			{
			       Value *v = U.get();
			      
				errs()<< *v << "\n";
			   
		        }
	         }
	}
      }
      return false;		
     
  }				 		
     	 	
    
  };
}

char UseDef::ID = 0;
static RegisterPass<UseDef> X("UseDef", "This is Use def  pass");

