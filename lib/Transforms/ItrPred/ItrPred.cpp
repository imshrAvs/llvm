#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/CFG.h"
using namespace llvm;
 
namespace
{
  // Hello - The first implementation, without getAnalysisUsage.
   struct ItrPred : public FunctionPass
   {
    
	static char ID; // Pass identification, replacement for typeid
    	ItrPred() : FunctionPass(ID) {}

	bool runOnFunction(Function &F) override{
	errs() << "Hello: ";
	errs() << F.getName() <<"\n";
	BasicBlock *target;
	int basicblockcounter = 0;
	for(Function::iterator bb = F.begin(), e = F.end(); bb !=e; bb++)
        {
	     basicblockcounter++;
             if(basicblockcounter == 4 )
		{
		 target = &*bb;
		 break;
		}
    	}
	
	for(pred_iterator PI = pred_begin(target), E = pred_end(target); PI != E;++PI)
	{
	 BasicBlock *pred= *PI;
	 errs() << "BasicBlock Name " << pred->getName() << "\n";
        }
      }
   };
}

char ItrPred::ID = 0;
static RegisterPass<ItrPred> X("ItrPred", "Iterate over the predecessor basicblocks of  bb");
