//===- Count_Opcodes.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Instructions.h" 
#include "llvm/IR/InstrTypes.h" 
#include "llvm/IR/CallSite.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Module.h"
#include <map>
#include <vector>
using namespace llvm;

//STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct CountFunction : public FunctionPass {
    		
    static char ID; // Pass identification, replacement for typeid
    CountFunction() : FunctionPass(ID) {}
    virtual bool runOnFunction(Function &F) override {
       
    int count=0;
      errs() << "Function: " << F.getName() << "\n";
      errs() << "Number of Arguments in a Function: " << F.arg_size() << "\n\n";
     	
      for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
      {
		    count++;
        errs() << " BasicBlock Name=" << bb->getName() << "\n";
        errs() << " The total number of instructions in a basic block named  " << bb->getName() << " = "<< bb->size() <<"\n" ;
		  
		 	if( pred_size(&*bb) == 1)
			{
			
				errs() << "The basic block containing one predecessor " << bb->getName() << "\n";
		  	
			}
			if ( pred_size(&*bb) > 1)
			{
				errs() << "The basic block containing multiple predecessor " << bb-> getName() << "\n" ;
			}

      errs() << *bb << "\n";
      Instruction *pi =  bb->getFirstNonPHI(); 
      IRBuilder<> Builder(pi);
      Type *Int32Ty = Builder.getInt32Ty();
      errs() << *bb << "\n";
      errs() << " The total number of instructions in a basic block named  " << bb->getName() << " after adding instruction is= "<< bb->size() <<"\n" ;
	}
      errs() << "The total number of basic block in a function is: " << count << "\n\n\n" ;
	return false;
      }
       
  };
}

char CountFunction::ID = 0;
static RegisterPass<CountFunction> X("CountFunction", "Counts bb,instructions per function");



namespace {
    // Hello2 - The second implementation with getAnalysisUsage implemented.
    struct Hello2 : public FunctionPass {
      std::vector<llvm::BasicBlock*> Vbb;    
      std::map< BasicBlock *,int > Signature; 
      std::map< BasicBlock *,int > dsig; 
      std::map< BasicBlock *,int > Dsig; 
      static char ID; // Pass identification, replacement for typeid
      Hello2() : FunctionPass(ID) {}
    
      bool runOnFunction(Function &F) override {
       int count=0;
       errs() << "Function" << F.getName() << "\n";
      // for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
       for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
       {
	  Vbb.push_back(&*bb);     
       }
	
       /*for(std::vector<BasicBlock*>::iterator V = Vbb.begin(), e = Vbb.end(); V != e; ++V)
       	{
          errs() << **V << "\n";
       	}*/
      
       
      // for(std::vector<BasicBlock*>::iterator bb = Vbb.begin(), e = Vbb.end(); bb != e; ++bb)
      for(BasicBlock* bb: Vbb)
       {
           Signature[bb]=count++;
       }
           
      // for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
      //for(std::vector<BasicBlock*>::iterator bb = Vbb.begin(), e = Vbb.end(); bb != e; ++bb)
     /* for(BasicBlock* bb: Vbb)
       {
          errs() << (bb)->getName() << " : " << Signature[bb] << "\n";
       
       
       errs() << "\n";
       }*/ 
       BasicBlock *Pbb;
       IRBuilder<> Builder((F.begin())->getFirstNonPHI()); 
        
       GlobalVariable *GV =
          new llvm::GlobalVariable(*F.getParent(),
                                   IntegerType::getInt32Ty((F.getContext())),
                                   false,
                                   llvm::GlobalValue::InternalLinkage,
                                   Builder.getInt32(0), "G");
        

       GlobalVariable *Dg =
          new llvm::GlobalVariable(*F.getParent(),
                                   IntegerType::getInt32Ty((F.getContext())),
                                   false,
                                   llvm::GlobalValue::InternalLinkage,
                                   Builder.getInt32(0), "D");
        

       Builder.CreateStore(Builder.getInt32(0), GV);
       Value *Diff;
       Value *Diff1;
       Value *Diff2;
       
       Module *M = F.getParent();
       Constant *c = M->getOrInsertFunction("__error", 
       FunctionType::getInt32Ty(F.getContext()), 
                          NULL);
       Function *error = cast<Function>(c);
      

      BasicBlock *entry = BasicBlock::Create(F.getContext(),"asd", &F);
      Builder.SetInsertPoint(entry);
      CallInst *memset_call = Builder.CreateCall(error);
      Value *rzero=Builder.getInt32(0);	
      Builder.CreateRet(rzero);
      for(BasicBlock* bb: Vbb)
      // for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
       {
         if( pred_size(bb) == 1)
         {
           dsig[bb]=Signature[bb] ^ Signature[(bb)->getSinglePredecessor()];
         }
	 else if( pred_size(bb) > 1)
         {
		 for(BasicBlock *Pred:predecessors(bb))
		 {
			Pbb=Pred;
			dsig[bb]=Signature[Pbb]^Signature[bb];
			break;	
		 }
		 for(BasicBlock *Pred:predecessors(bb))
		 {
		 
          		Builder.SetInsertPoint((Pred)->getFirstNonPHI());
                        Dsig[Pred]=Signature[Pbb]^Signature[Pred];
			Builder.CreateStore(Builder.getInt32(Dsig[Pred]),Dg);
		 }

	  	 
	  }
       }

      for(BasicBlock* bb:Vbb)
      {
	      errs() << bb->getName() << ":" << Dsig[bb] << "\n";
      }

           
      //for(std::vector<BasicBlock*>::iterator bb = Vbb.begin(), e = Vbb.end(); bb != e; ++bb)
      /*for(BasicBlock* bb: Vbb)
      // for(Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
       {
          errs() << (bb)->getName() << " : " << dsig[bb] << "\n";
       }*/
       
     
      //for(std::vector<BasicBlock*>::iterator bb = Vbb.begin(), e = Vbb.end(); bb != e; ++bb)
      for(BasicBlock* bb: Vbb)
      {
           
        if ( pred_size(bb)==1)
        {
          Builder.SetInsertPoint((bb)->getFirstNonPHI());
          LoadInst *LI = Builder.CreateLoad(GV);
          Diff=Builder.CreateXor(Builder.getInt32(dsig[bb]),LI);
          Builder.CreateStore(Diff,GV);

          Value *fail=Builder.CreateICmpNE(Diff,Builder.getInt32(Signature[bb]), "failure" );
	 // Builder.SetInsertPoint((dyn_cast<Instruction>(fail))->getNextNode());
	  BasicBlock *dd=(bb)->splitBasicBlock(dyn_cast<Instruction>(fail)->getNextNode(), "split");
	  (bb->getTerminator())->eraseFromParent();
          Builder.SetInsertPoint((bb));
          Builder.CreateCondBr(fail,entry,dd);
         // errs() << *dd << "\n";
	}
      	else if( pred_size(bb)>1)
	 {
		 
		/* for(BasicBlock *Pred:predecessors(bb))
		 {
			Pbb=Pred;
			break;	
		 }
		 for(BasicBlock *Pred:predecessors(bb))
		 {
		 
          		Builder.SetInsertPoint((Pred)->getFirstNonPHI());
                        Dsig[Pred]=Signature[Pbb]^Signature[Pred];
			Builder.CreateStore(Builder.getInt32(Dsig[Pred]),Dg);
		 }*/
          Builder.SetInsertPoint((bb)->getFirstNonPHI());
       	  LoadInst *LI = Builder.CreateLoad(GV);
	  LoadInst *DI = Builder.CreateLoad(Dg);
	  //errs() <<"\n"<< *Dg << "\n";
	  Diff=Builder.CreateXor(Builder.getInt32(dsig[bb]),LI);
	  Diff1=Builder.CreateXor(DI,Diff);
	  Builder.CreateStore(Diff1,GV);

	  Value *fail=Builder.CreateICmpNE(Diff1,Builder.getInt32(Signature[bb]), "failure" );
	  BasicBlock *dd=(bb)->splitBasicBlock(dyn_cast<Instruction>(fail)->getNextNode(), "split");
	  (bb->getTerminator())->eraseFromParent();
          Builder.SetInsertPoint((bb));
          Builder.CreateCondBr(fail,entry,dd);
          //errs() << *dd << "\n";
 	 }
      	}
       
       errs() << *F.getParent() << "\n";

       return false;
      }
    
     
   };
 
} 
char Hello2::ID = 0;
static RegisterPass<Hello2>
Y("Signature", "Hello World Pass (with getAnalysisUsage implemented)");
