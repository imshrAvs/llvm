//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;

#define DEBUG_TYPE "hello"

namespace {
// Hello - The first implementation, without getAnalysisUsage.
struct FunctionCompare : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  std::vector<Function *> Func;
  std::map<BasicBlock *, BasicBlock *> basic, inProcess;
  std::map<Value *, Value *> ax;
  std::vector<Type *> arg1;
  std::vector<Type *> arg2;
  std::vector<Value *> varg1;
  std::vector<Value *> varg2;
  std::vector<Instruction *> ins1;
  std::vector<Instruction *> ins2;

  FunctionCompare() : ModulePass(ID) {}

  bool runOnModule(Module &M) override {

    ax.clear();
    for (Function &F : M) {
      Func.push_back(&F);
    }

    Function *f1 = Func[0];
    Function *f2 = Func[1];

    Type *rt1 = f1->getReturnType();
    Type *rt2 = f2->getReturnType();

    int pc1 = f1->arg_size();
    int pc2 = f2->arg_size();

    int bc1, bc2;
    for (Function::arg_iterator AI = f1->arg_begin(), AE = f1->arg_end();
         AI != AE; ++AI) {
      bc1++;
      arg1.push_back(AI->getType());
      varg1.push_back(AI);
    }

    for (Function::arg_iterator AI = f2->arg_begin(), AE = f2->arg_end();
         AI != AE; ++AI) {
      bc2++;
      arg2.push_back(AI->getType());
      varg2.push_back(AI);
    }

    for (unsigned int i = 0; i < varg1.size(); ++i) {
      ax[varg1[i]] = varg2[i];
    }

    BasicBlock *bb1 = &f1->getEntryBlock();
    BasicBlock *bb2 = &f2->getEntryBlock();
    bool fc;
    if (rt1 == rt2) {
      if (pc1 == pc2) {
        if (arg1 == arg2) {
          inProcess[bb1] = bb2;
          fc = inst_compare(bb1, bb2);
          inProcess.erase(bb1);
        }
      }
    }

    if (fc == true) {
      errs() << "functions are same"
             << "\n";
    } else {
      errs() << "not same"
             << "\n";
    }

    // errs() << *f1 << *f2 << "\n";
    return false;
  }

  bool inst_compare(BasicBlock *B1, BasicBlock *B2, bool skip_br = false) {
    // if in_processBBMap[B1] == B2
    int flagg = 1;
    for (BasicBlock::iterator i = B1->begin(), j = B2->begin();
         i != B1->end(), j != B2->end(); ++i, ++j) {

      // if inst is phinode
      // for all incoming values
      //   if incoming block is in map? continue
      //   else if (the incoming block has one succussor), call inst_comapare
      //   with latch set to true;
      if (isa<PHINode>(i) && isa<PHINode>(j)) {
        if (dyn_cast<PHINode>(i)->getNumIncomingValues() ==
            dyn_cast<PHINode>(j)->getNumIncomingValues()) {
          for (int k = 0; k < dyn_cast<PHINode>(i)->getNumIncomingValues();
               k++) {
            
            if (basic[dyn_cast<PHINode>(i)->getIncomingBlock(k)] ==
                dyn_cast<PHINode>(j)->getIncomingBlock(k)) {
                if (isa<Constant>(dyn_cast<PHINode>(i)->getIncomingValue(k)) &&
              isa<Constant>(dyn_cast<PHINode>(j)->getIncomingValue(k)) ) {
                  Constant *val1 = dyn_cast<Constant>(dyn_cast<PHINode>(i)->getIncomingValue(k));
                  Constant *val2 = dyn_cast<Constant>(dyn_cast<PHINode>(j)->getIncomingValue(k));
                  if (val1 == val2) {
                        continue;
                  } else {
                    return false;
                  }
                 
              } else if(ax[dyn_cast<PHINode>(i)->getIncomingValue(k)]==dyn_cast<PHINode>(j)->getIncomingValue(k)) {
                continue;
            } else {
               return false;
            }
            continue;
          } else if (dyn_cast<PHINode>(i)
                           ->getIncomingBlock(k)
                           ->getSingleSuccessor()) {
              bool tmp = inst_compare(dyn_cast<PHINode>(i)->getIncomingBlock(k),
                           dyn_cast<PHINode>(j)->getIncomingBlock(k), true);
             if (!tmp) {
               return false;
             }
            } else if (inProcess[dyn_cast<PHINode>(i)->getIncomingBlock(k)] ==
                       dyn_cast<PHINode>(j)->getIncomingBlock(k)) {
              continue;
            } else {
              return false;
           }
          
          }
        } else {
            return false;
        }
                  // Add the phi to ax map
        //errs() << "shravan 1st" << "\n";
        //print_map();
        ax[dyn_cast<Value>(i)]=dyn_cast<Value>(j);
        //errs() << "shravan 2nd" << "\n";
        //print_map();
        continue;
      }

      if (isa<BranchInst>(i) && isa<BranchInst>(j)) {
        if (skip_br)
          return true;
        BranchInst *BI1 = dyn_cast<BranchInst>(i);
        BranchInst *BI2 = dyn_cast<BranchInst>(j);
        if (BI1->isConditional() && BI2->isConditional()) {
          // Check for 0th operand in map, if not found return false
          // Get 0th successor of both Function's BB and check in the BBMap, if
          // it is not there call inst_compare if it is there in the map, then
          if (ax[i->getOperand(0)] == j->getOperand(0)) {
            bool t1 = false, t2 = false;
            if (basic[i->getSuccessor(0)] == j->getSuccessor(0)) {
              t1 = true;
            } else {
              inProcess[i->getSuccessor(0)] = j->getSuccessor(0);
              t1 = inst_compare(i->getSuccessor(0), j->getSuccessor(0));
              inProcess.erase(i->getSuccessor(0));
              if (t1) {
                basic[i->getSuccessor(0)] = j->getSuccessor(0);
              } // add to map
            }
            // with succussor-1 set t2
            if (basic[i->getSuccessor(1)] == j->getSuccessor(1)) {
              t2 = true;
            } else {
              inProcess[i->getSuccessor(1)] = j->getSuccessor(1);
              t2 = inst_compare(i->getSuccessor(1), j->getSuccessor(1));
              inProcess.erase(i->getSuccessor(1));
              if (t2) {
                basic[i->getSuccessor(1)] = j->getSuccessor(1);
              }
            }
            return t1 && t2;
          } else {
            return false;
          }

        } else if (BI1->isUnconditional() && BI2->isUnconditional()) {
          bool t3 = false;
          if (basic[i->getSuccessor(0)] == j->getSuccessor(0)) {
            t3 = true;
          } else {
            // in_processBBMap[i->succ(0)] = j->succ(0);
            inProcess[i->getSuccessor(0)] = j->getSuccessor(0);
            t3 = inst_compare(i->getSuccessor(0), j->getSuccessor(0));
            inProcess.erase(i->getSuccessor(0));
            // Remove i->succ(0), j->succ(0) from in_processBBmap;
            if (t3) {
              basic[i->getSuccessor(0)] = j->getSuccessor(0);
            }
          }

          return t3;
        }
      }
      if (i->getOpcodeName() == j->getOpcodeName()) {
        flagg = flagg && 1;
        // ax[dyn_cast<Value>(i)]=dyn_cast<Value>(j);
        for (int k = 0; k < i->getNumOperands(); k++) {
          if (isa<Constant>(i->getOperand(k)) &&
              isa<Constant>(j->getOperand(k))) {
            Constant *val1 = dyn_cast<Constant>(i->getOperand(k));
            Constant *val2 = dyn_cast<Constant>(j->getOperand(k));
            if (val1 == val2) {
              // ax[dyn_cast<Value>(i)]=dyn_cast<Value>(j);
              flagg = flagg && 1;
              continue;
            } else {
              flagg = flagg && 0;
              return false;
            }
          } else if (ax[i->getOperand(k)] == j->getOperand(k)) {
            // return true;
            // break;
            flagg = flagg && 1;
            //errs() << "3rd " << "\n";
            //print_map();
            continue;
          } else if (isa<Instruction>(i->getOperand(k)) &&
                     isa<Instruction>(j->getOperand(k)) &&
                     inProcess[dyn_cast<Instruction>(i->getOperand(k))
                                   ->getParent()] ==
                         dyn_cast<Instruction>(j->getOperand(k))->getParent()) {
            //errs() << "shravan 4th" << "\n";
            //print_map();
            flagg = flagg && 1;
            continue;
          } else {
            flagg = flagg && 0;
            return false;
          }
        }
      } else {
        flagg = flagg && 0;
        return false;
      }

      if (flagg == 1) {
        ax[dyn_cast<Value>(i)] = dyn_cast<Value>(j);
      } else {
        return false;
      }
    }
    return true;
  }

void print_map()
{
  std::map<Value *,Value *>::iterator i =ax.begin();
  std::map<Value *,Value *>::iterator e = ax.end();
  while (i!=e)
  {
    if (i->first && i->second)
      errs() << *i->first<< ":" << *i->second << "\n";
    else
      errs() << *i->first<< ":" << "nullptr" << "\n";
    i++;
  }
}

  // return true;
};
} // namespace

char FunctionCompare::ID = 0;
static RegisterPass<FunctionCompare> X("FunctionCompare",
                                       "Comparing 2 functions are same or not");
