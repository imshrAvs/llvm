//===- Count_Opcodes.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;
#include <map>
#define DEBUG_TYPE "opCounter"

//STATISTIC(HelloCounter, "Counts number of functions greeted");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct CountOp : public FunctionPass {
    std::map<std::string, int> opCounter;
    		
    static char ID; // Pass identification, replacement for typeid
    CountOp() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &F) override {
      //++HelloCounter;
      errs() << "Function" << F.getName() << "\n";
      for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
	for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i !=e; ++i) {
           if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
		opCounter[i->getOpcodeName()] = 1;
	} else {
	  opCounter[i->getOpcodeName()] +=1;
	}
      }
   }
   
   std::map <std::string, int>::iterator i = opCounter.begin();
   std::map <std::string, int>::iterator e = opCounter.end();
   while (i !=e) {
	errs() << i->first << ":" << i->second << "\n";
	i++;
   }
   errs() << "\n";
   opCounter.clear();
   return false;
  }				 		
     	 	
   //rrs().write_escaped(F.getName()) << '\n';
    //  return false;
    
  };
}

char CountOp::ID = 0;
static RegisterPass<CountOp> X("opCounter", "Counts opcodes per function");

