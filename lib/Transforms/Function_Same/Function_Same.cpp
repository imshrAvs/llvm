//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;

#define DEBUG_TYPE "hello"

namespace {
// Hello - The first implementation, without getAnalysisUsage.
struct Function_Same : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  std::vector<Function *> Func;
  std::vector<Type *> arg1;
  std::vector<Type *> arg2;
  std::vector<Value *> varg1;
  std::vector<Value *> varg2;
  std::vector<BasicBlock *> bb1;
  std::vector<BasicBlock *> bb2;
  std::vector<Instruction *> ins1;
  std::vector<Instruction *> ins2;
  std::map<Value *, Value *> ax;
  Function_Same() : ModulePass(ID) {}

  bool runOnModule(Module &M) override {

    for (Function &F : M) {
      Func.push_back(&F);
    }

    Function *f1 = Func[0];
    Function *f2 = Func[1];

    // errs() << *f1 << "\n";
    //  errs() << *f2 << "\n";

    Type *rt1 = f1->getReturnType();
    Type *rt2 = f2->getReturnType();

    // errs() << *rt1 << "\n";
    // errs() << *rt2 << "\n";

    int pc1 = f1->arg_size();
    int pc2 = f2->arg_size();

    // errs() << pc1 << "\t" << pc2 << "\n";
    int bc1, bc2;

    for (Function::arg_iterator AI = f1->arg_begin(), AE = f1->arg_end();
         AI != AE; ++AI) {
      bc1++;
      arg1.push_back(AI->getType());
      varg1.push_back(AI);
    }

    /*for (Type *p:arg1) {
       errs()  <<*p << "\n";
    }*/

    /* for (Value *p:varg1) {
        errs()  <<*p << "\n";
     }*/

    for (Function::arg_iterator AI = f2->arg_begin(), AE = f2->arg_end();
         AI != AE; ++AI) {
      bc2++;
      arg2.push_back(AI->getType());
      varg2.push_back(AI);
    }

    /*for (Type *p:arg2) {
       errs()  <<*p << "\n";
    }*/

    /*for (Value *p:varg2){
      errs() << *p << "\n";
    }*/

    for (unsigned int i = 0; i < varg1.size(); ++i) {
      ax[varg1[i]] = varg2[i];
    }

    std::map<Value *, Value *>::iterator i = ax.begin();
    std::map<Value *, Value *>::iterator e = ax.end();
    while (i != e) {
      //errs() << *i->first << ":" << *i->second << "\n";
      i++;
    }

    // int b_size1=(f1->getBasicBlockList())->size();
    for (BasicBlock &bb : f1->getBasicBlockList()) {

      bb1.push_back(&bb);
    }

    for (BasicBlock &bb : f2->getBasicBlockList()) {
      bb2.push_back(&bb);
    }

    int c1, c2;
    for (BasicBlock *p : bb1) {
      for (Instruction &I : *p) {
        c1++;
        ins1.push_back(&I);
      }
      //      errs() << *p<< "\n" << c1 << "\n";
    }

    for (BasicBlock *p : bb1) {
      //errs() << *p;
    }

    for (BasicBlock *p : bb2) {
      for (Instruction &I : *p) {
        c2++;
        ins2.push_back(&I);
      }
      //        errs() << *p<< "\n" << c2 << "\n";
    }

    for (BasicBlock *p : bb2) {
    //  errs() << *p;
    }

    errs() << "************ instructions of basic block 1 *********\n";
    for (Instruction *i : ins1) {
  //    errs() << *i << "\n";
    }

    errs() << "************ instructions of basic block 2 *********\n";
    for (Instruction *i : ins2) {
//      errs() << *i << "\n";
    }
    // errs() << bb2.size() << "\n";

    bool flag = 1;
    /*if( rt1 == rt2 ){
      if(pc1 == pc2){
        if(arg1 == arg2){
          if(bc1==bc2)
            if(c1==c2){
              for( int i=0;i<c1;++i){
              if(ins1[i]->getOpcodeName() == ins2[i]->getOpcodeName())
              {
                for(int j=0;j< ins1[i]->getNumOperands();j++){
                //if(ax[ins1[i]->getOperand(i)] == ins2[i]->getOperand(i)){
                if(ax[ins1[i]->getOperand(j)] == ins2[i]->getOperand(j)){
                 //}// else if it is constat, ins2 is also a cont then compare
      both if(isa<Constant>(ins1[i]->getOperand(j))){ if(ins1[i]->getOperand(j)
      == ins2[i]->getOperand(j)) continue;
                 //   flag = flag && 1;
                    }
                //}
                else {
                   flag = flag && 0;
                   break;
                 }
                }
                // if the flag is still 1 then add the instruction in
      ax[f1-inst] = f2-inst if(flag==1){ ax[ins1[i]]=ins2[i];
                }
                else
                  break;
                // else exit
                //
              }
          // break;
            }

          }
          }
        }
      }
      */

    if (rt1 == rt2) {
      if (pc1 == pc2) {
        if (arg1 == arg2) {
          if (bc1 == bc2)
            if (c1 == c2) {
              for (int i = 0; i < c1; ++i) {
                if (isa<BranchInst>(ins1[i]) && isa<BranchInst>(ins2[i])) {
                  ax[ins1[i]] = ins2[i];
                  continue;
                }/* else if (isa<PHINode>(ins1[i]) && isa<PHINode>(ins1[i])) {
                  ax[ins1[i]] = ins2[i];
                  continue;
                }*/ else {
                  if (ins1[i]->getOpcodeName() == ins2[i]->getOpcodeName()) {
                    ax[ins1[i]] = ins2[i];
                    if(isa<PHINode>(ins1[i]) && isa<PHINode>(ins1[i])){
                      for(int k=0;k < cast<PHINode>(ins1[i])->getNumIncomingValues();k++){
                        if( (cast<PHINode>(ins1[i])->getIncomingValue(k) == cast<PHINode>(ins2[i])->getIncomingValue(k)) && (cast<PHINode>(ins1[i])->getIncomingBlock(k) == cast<PHINode>(ins2[i])->getIncomingBlock(k))){
                          errs() << cast<PHINode>(ins1[i])->getIncomingValue(k) << "\n" << cast<PHINode>(ins2[i])->getIncomingValue(k) << "\n";
                        flag=flag&&1; 
                        }
                        else {
                          flag= flag&& 0;
                          break;
                        }
                      }
                    }
                    for (int j = 0; j < ins1[i]->getNumOperands(); j++) {
                      if (isa<Constant>(ins1[i]->getOperand(j)) &&
                          isa<Constant>(ins2[i]->getOperand(j))) {
                        Constant *val1 =
                            dyn_cast<Constant>(ins1[i]->getOperand(j));
                        Constant *val2 =
                            dyn_cast<Constant>(ins2[i]->getOperand(j));
                        if (val1 == val2) {
                          ax[ins1[i]] = ins2[i];
                        }
                        else
                        {
                          flag=flag && 0;
                          break;
                        }
                      } else if (ax[ins1[i]->getOperand(j)] ==
                                 ins2[i]->getOperand(j)) {
                        flag = flag && 1;
                      } else {
                        flag = flag && 0;
                        break;
                      }
                    }
                  }
                }
              }
            } else {
              flag = flag && 0;
            }
        }
      }
    }

    if (flag == 1) {
      errs() << "functions are same"
             << "\n";
    } else
      errs() << "not same"
             << "\n";

    return false;
  }
};
} // namespace

char Function_Same::ID = 0;
static RegisterPass<Function_Same> X("function_same",
                                     "Comparing 2 functions are same or not");
